package com.demo.lms.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.lms.dto.StudentRequestDto;
import com.demo.lms.dto.StudentResponseDto;
import com.demo.lms.entity.Student;
import com.demo.lms.repository.RegistrationRepository;


@SpringBootTest
class RegistrationServiceTest {
	
	
	@InjectMocks
	private RegistrationService registrationService;
	
	@Mock
	private RegistrationRepository registrationRepository;

	@Test
	public void testRegisterStudent() {
		StudentRequestDto requestDto = new StudentRequestDto();
		requestDto.setEmail("sid@gmai.com");
		requestDto.setFirstName("sid");
		requestDto.setLastname("mudhol");
		requestDto.setMobileNo(998733);
		requestDto.setPassword("password");
		StudentResponseDto responseDto = new StudentResponseDto();
		responseDto.setMessage("registered");
		responseDto.setStudentId(1);
		Mockito.when(registrationRepository.save(Mockito.any())).thenReturn(responseDto);
		StudentResponseDto dto = registrationService.registerStudent(requestDto);
		assertEquals("registered", dto.getMessage());
	}

}
