/*
 * package com.demo.lms.service;
 * 
 * import static org.assertj.core.api.Assertions.assertThat; import static
 * org.mockito.Mockito.when;
 * 
 * import java.time.LocalDate; import java.util.ArrayList; import
 * java.util.List;
 * 
 * import org.junit.Before; import org.junit.Test; import
 * org.junit.runner.RunWith; import org.mockito.InjectMocks; import
 * org.mockito.Mock; import
 * org.springframework.boot.test.context.SpringBootTest; import
 * org.springframework.test.context.junit4.SpringRunner;
 * 
 * import com.demo.lms.entity.Course; import
 * com.demo.lms.exception.CourseNotFoundException; import
 * com.demo.lms.repository.StudentRepository;
 * 
 * @SpringBootTest
 * 
 * @RunWith(value = SpringRunner.class) public class StudentServiceTests {
 * 
 * @InjectMocks StudentService studentService;
 * 
 * @Mock StudentRepository studentRepository;
 * 
 * List<Course> courses ;
 * 
 * @Before public void setup() { courses = new ArrayList<>(); LocalDate start =
 * LocalDate.of(2020, 03, 12); LocalDate end = LocalDate.of(2020, 05, 12);
 * Course course = new Course(1L,"xyx","java",start,end); courses.add(course); }
 * 
 * @Test public void testSearch() throws CourseNotFoundException {
 * when(studentService.search(courses.get(0).getCourseName())).thenReturn(
 * courses.get(0)); //assertThat(new Course(),courses.get(0)); } }
 */