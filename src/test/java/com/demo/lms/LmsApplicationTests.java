package com.demo.lms;

import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.lms.controller.StudentController;
import com.demo.lms.entity.Student;
import com.demo.lms.service.StudentService;

@SpringBootTest
class LmsApplicationTests {
	
	@Mock
	private StudentController controller;

	@Test
	void contextLoads() {
	}


	@Mock
	private StudentService studentService;
	@Before
	public void setups() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testStudentLogInValidation() {
		Optional<Student> student = null;
		doReturn(student).when(studentService).loginValidation("raj@gmail.com", "raja1233");
		//controller.studentLogInValidation("raj@gmail.com", "raja1233");
	}
}
