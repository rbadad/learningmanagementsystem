package com.demo.lms.constants;

public class Constants {

	public static final String 	COURSE_NOT_FOUND = "Requested course is not available";
	public static final String 	COURSE_NAME_EMPTY = "Course name should not be empty";
}
