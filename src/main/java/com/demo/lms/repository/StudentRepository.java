package com.demo.lms.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.lms.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{

	@Query(value = "select * FROM STUDENT s WHERE s.email = :email AND s.password = :password", nativeQuery = true)
	public Optional<Student> findByEmailAndRolePasswordNative(@Param("email") String email, @Param("password") String password);

}
