package com.demo.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.lms.entity.Enrollment;

public interface EnrollmentRespository extends JpaRepository<Enrollment, Long>{

}
