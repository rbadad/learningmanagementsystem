package com.demo.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.lms.entity.Enrollment;
import com.demo.lms.entity.Student;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Long>{

	public List<Enrollment> findByStudent(Student student);

	//@Query("select * from Enrollment e where e.course.cId =:courseId and e.student.studentId")
	//public Enrollment findByCourseIdAndStudentId(@PathParam("courseId") Long courseId,@PathParam("studentId") Long studentId);
}
