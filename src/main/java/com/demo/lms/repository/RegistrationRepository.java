package com.demo.lms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.demo.lms.entity.Student;

/**
 * This class act as repository for student registration.
 * @author Sidramesh Mudhol
 *
 */
@Repository
public interface RegistrationRepository extends CrudRepository<Student, Long> {
	
	

}
