package com.demo.lms.service;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.lms.entity.Course;
import com.demo.lms.entity.Enrollment;
import com.demo.lms.entity.Student;
import com.demo.lms.repository.EnrollmentRepository;
import com.demo.lms.repository.RegistrationRepository;

@Service
public class EnrollmentService {
	
	@Autowired
	private EnrollmentRepository repository;
	
	@Autowired
	private RegistrationRepository rrepo;

	public List<?> getStudentEnrollmentDetails(String studentId) {
		List<Enrollment> enrl = null;
		List<Course> pastCouurseDetails = null;
		Optional<Student> student = rrepo.findById(Long.parseLong(studentId));
		if(student.isPresent()) {
			enrl = repository.findByStudent(student.get());
			if(null!=enrl) {
				for (Iterator iterator = enrl.iterator(); iterator.hasNext();) {
					Enrollment enrollment = (Enrollment) iterator.next();
					LocalDate past = enrollment.getCourse().getCourseEndDate();
					LocalDate present = LocalDate.now();
					if(past.isBefore(present)) {
						
					}
				}
				
			}
			 enrl.forEach(System.out::print);
		}
		
		return null;
	}

}
