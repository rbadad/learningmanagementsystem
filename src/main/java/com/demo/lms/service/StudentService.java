package com.demo.lms.service;

import java.time.LocalDate;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.lms.dto.EnrollmentDto;
import com.demo.lms.entity.Course;
import com.demo.lms.entity.Enrollment;
import com.demo.lms.entity.Student;
import com.demo.lms.exception.CourseNameEmptyException;
import com.demo.lms.exception.CourseNotFoundException;
import com.demo.lms.repository.CourseRepository;
import com.demo.lms.repository.EnrollmentRepository;

import com.demo.lms.entity.Student;
import com.demo.lms.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	EnrollmentRepository enrollmentRepository;

	/**
	 * searching the courses based on course name.
	 * @param courseName
	 * @return Course Object
	 * @throws CourseNotFoundException
	 */
	public Course search(String courseName) throws CourseNotFoundException {
		Course course = courseRepository.findByCourseName(courseName);
		if(Objects.isNull(course)) {
			throw new CourseNotFoundException();
		}
		return course;
	}

	/**
	 * student enrolls for the course.
	 * @param enrollmentDto
	 * @return Enrollment Object
	 * @throws CourseNameEmptyException 
	 */
	public Enrollment studentEnrollment(EnrollmentDto enrollmentDto) throws CourseNameEmptyException {
		Enrollment enrolledObj = null;
		String courseName = enrollmentDto.getCourseName();
		if(courseName.isEmpty()) {
			throw new CourseNameEmptyException();
		}
		
		Long studentId = enrollmentDto.getStudentId();
		Course course = courseRepository.findByCourseName(courseName);
		Long courseId = course.getCId();
		Enrollment enrol = enrollmentRepository.findByCourseIdAndStudentId(courseId, studentId);
		if(!Objects.isNull(enrol)) {
			
		}
		Enrollment enrollment = new Enrollment();
		Student student = new Student();
		Course courseObj = new Course();
		student.setSId(studentId);
		courseObj.setCId(courseId);
		enrollment.setStudent(student);
		enrollment.setCourse(courseObj);
		LocalDate date  = LocalDate.now();
		enrollment.setEnrollmentDate(date);
		enrolledObj = enrollmentRepository.save(enrollment);
		return enrolledObj;
	}
	
	private StudentRepository studentRepository;
	public Optional<Student> loginValidation(String email, String password) {
		return studentRepository.findStudentByEmailAndPasswordParams(email, password);
		
	}
}
