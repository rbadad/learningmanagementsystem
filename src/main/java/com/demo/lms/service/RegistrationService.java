package com.demo.lms.service;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.lms.dto.StudentRequestDto;
import com.demo.lms.dto.StudentResponseDto;
import com.demo.lms.entity.Student;
import com.demo.lms.repository.RegistrationRepository;


/**
 * 
 * This is service class for registering student.
 * @author sidramesh Mudhol
 *
 */
@Service
public class RegistrationService {
	
	/**
	 *  log.
	 */
	private static final Logger log = LoggerFactory.getLogger(RegistrationService.class);
	
	/**
	 * registrationRepository
	 */
	@Autowired
	private RegistrationRepository registrationRepository;
	
	/**
	 * This method is used for registering students.
	 * @param requestDto, may not be null.
	 * @return StudentResponseDto, may not be null.
	 * 
	 */
	public StudentResponseDto registerStudent(StudentRequestDto requestDto) {
		log.info("inside registerStudent service method");
		Student studentEntity = null;
		StudentResponseDto responseDto = new StudentResponseDto();
		Student student =new Student();
		student.setFirstName(requestDto.getFirstName());
		student.setLastName(requestDto.getLastname());
		student.setPassword(requestDto.getPassword());
		student.setMobileNo(requestDto.getMobileNo());
		student.setEmail(requestDto.getEmail());
		try {
		studentEntity = registrationRepository.save(student);
		} catch (Exception ex) {
			log.error("error while persisting student entity " + ex.getMessage());
			throw new PersistenceException("registering is unsuccessful");
		}
		if (studentEntity != null) {
			log.debug("student with his id "+ studentEntity.getSId());
			responseDto.setStudentId(studentEntity.getSId());
			responseDto.setMessage("student registered successfully");
		}
		log.info("exiting registerStudent service method");
		return responseDto;
		
	}

}
