package com.demo.lms.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.lms.constants.AppConstants;
import com.demo.lms.dto.AppResponseDto;
import com.demo.lms.dto.EnrollmentDto;
import com.demo.lms.dto.StudentLoginRequestDto;
import com.demo.lms.entity.Course;
import com.demo.lms.entity.Enrollment;
import com.demo.lms.entity.Student;
import com.demo.lms.exception.CourseNameEmptyException;
import com.demo.lms.exception.CourseNotFoundException;
import com.demo.lms.service.EnrollmentService;
import com.demo.lms.service.StudentService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/students")
public class StudentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private EnrollmentService enrollmentService;
	
	/**
	 * 
	 * @param courseName
	 * @return Course Object
	 * @throws CourseNotFoundException
	 */
	
	@GetMapping("/search/{courseName}")
	public ResponseEntity<Course> search(@PathVariable String courseName) throws CourseNotFoundException {
		Course course = studentService.search(courseName);
		return new ResponseEntity<>(course,HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param enrollmentDto
	 * @return
	 * @throws CourseNameEmptyException
	 */
	@PostMapping("/enrollment")
	public ResponseEntity<Enrollment> studentEnrollment(@RequestBody EnrollmentDto enrollmentDto) throws CourseNameEmptyException {
		Enrollment enrollment = studentService.studentEnrollment(enrollmentDto);
		return new ResponseEntity<>(enrollment,HttpStatus.CREATED);
	}
	
	@PostMapping("/login")
	@ApiOperation("Student login")
	public AppResponseDto studentLogInValidation(@RequestBody StudentLoginRequestDto request) {
		LOGGER.info("student login cred===" + request.getEmail()+","+request.getPassword());
		AppResponseDto response = new AppResponseDto();
		LOGGER.info("!!!!!!!!!!Before calling service classS!!!!!!!!!");
		Optional<Student> student = studentService.loginValidation(request.getEmail(), request.getPassword());
		if(student.isPresent()) {
			LOGGER.debug("student.get().== " + student.get());
			response.setResponseCode("1");
			response.setStatus(AppConstants.LOGIN_SUCCESS);
			response.setErrorMessage("");
		}else {
			LOGGER.warn("please enter the correct username and password");
			response.setResponseCode("0");
			response.setStatus(AppConstants.LOGIN_FAILURE);
			response.setErrorMessage("please enter correct username and password");
		}
		LOGGER.info("************************After service call**********************");
		return response;
	}
	
	@GetMapping("/enrollment/{studentId}")
	public List<?> getEnrollmentDetails(@PathVariable("studentId") String studentId) {
		
		return enrollmentService.getStudentEnrollmentDetails(studentId);
		
	}
	

}
