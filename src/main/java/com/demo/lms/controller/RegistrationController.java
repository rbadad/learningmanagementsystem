package com.demo.lms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.lms.dto.StudentRequestDto;
import com.demo.lms.dto.StudentResponseDto;
import com.demo.lms.service.RegistrationService;

import io.swagger.annotations.ApiOperation;

/**
 * This class act as controller for student registration functionality.
 * @author Sidramesh Mudhol 
 *
 */
@RestController
@RequestMapping("/api")
public class RegistrationController {
	
	/**
	 * log.
	 */
	private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);

	/**
	 * registrationService.
	 */
	@Autowired
	private RegistrationService registrationService;
	
	
	/**
	 * This is a controller method for student registration. 
	 * @param requestDto, may not be null.
	 * @return StudentResponseDto, may not be null.
	 */
	@PostMapping("/registration")
	@ApiOperation("Student Registration")
	public ResponseEntity<StudentResponseDto> registerStudent(@RequestBody StudentRequestDto requestDto) {
		log.info("inside registerStudent() method ");
		log.debug("student first name "+ requestDto.getFirstName());
		StudentResponseDto response = registrationService.registerStudent(requestDto);
		log.info("exiting registerStudent() method");
		return new ResponseEntity<StudentResponseDto>(response, HttpStatus.OK);
	}
	
	
	

}
