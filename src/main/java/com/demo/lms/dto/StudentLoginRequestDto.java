package com.demo.lms.dto;

public class StudentLoginRequestDto {
	
	private String email;
	private String password;
	public StudentLoginRequestDto() {
		super();
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
