package com.demo.lms.dto;

public class AppResponseDto {
	
	private String responseCode;
	private String status;
	private String errorMessage;
	public AppResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AppResponseDto(String responseCode, String status, String errorMessage) {
		super();
		this.responseCode = responseCode;
		this.status = status;
		this.errorMessage = errorMessage;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
