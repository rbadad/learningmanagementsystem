package com.demo.lms.dto;

import lombok.Data;

@Data
public class EnrollmentDto {

	private String courseName;
	private Long studentId;
}
