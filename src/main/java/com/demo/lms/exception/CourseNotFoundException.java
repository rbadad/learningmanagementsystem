package com.demo.lms.exception;

public class CourseNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CourseNotFoundException() {
		
	}
	
	public CourseNotFoundException(String msg) {
		super(msg);
	}

}
