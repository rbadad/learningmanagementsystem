package com.demo.lms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.demo.lms.constants.Constants;

@ControllerAdvice
public class GlobalExceptionHandlerController {

	@ExceptionHandler(value = CourseNotFoundException.class)
	public ResponseEntity<String> handleCourseNotFoundException() {
		return new ResponseEntity<>(Constants.COURSE_NOT_FOUND,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = CourseNameEmptyException.class)
	public ResponseEntity<String> handleCourseNameEmptyException() {
		return new ResponseEntity<>(Constants.COURSE_NAME_EMPTY,HttpStatus.NOT_FOUND);
	}
	
}

