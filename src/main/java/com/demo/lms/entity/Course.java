package com.demo.lms.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "COURSE")
@Data
public class Course implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "COURSE_ID")
	private Long cId;
	@Column(name = "COURSE_NAME")
	private String courseName;
	@Column(name = "COURSE_CATEGORY")
	private String courseCatagory;
	@Column(name = "COURSE_START_DATE")
	private LocalDate courseStartDate;
	@Column(name = "COURSE_END_DATE")
	private LocalDate courseEndDate;
	
	
	public Course() {
		super();
	}
	
	public Course(Long cId,String courseName,String courseCatagory,LocalDate courseStartDate,LocalDate courseEndDate) {
		this.cId =cId;
		this.courseCatagory =courseCatagory;
		this.courseEndDate = courseEndDate;
		this.courseName = courseName;
		this.courseStartDate = courseStartDate;
	}

	public Long getcId() {
		return cId;
	}

	public void setcId(Long cId) {
		this.cId = cId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCatagory() {
		return courseCatagory;
	}

	public void setCourseCatagory(String courseCatagory) {
		this.courseCatagory = courseCatagory;
	}

	public LocalDate getCourseStartDate() {
		return courseStartDate;
	}

	public void setCourseStartDate(LocalDate courseStartDate) {
		this.courseStartDate = courseStartDate;
	}

	public LocalDate getCourseEndDate() {
		return courseEndDate;
	}

	public void setCourseEndDate(LocalDate courseEndDate) {
		this.courseEndDate = courseEndDate;
	}
}

