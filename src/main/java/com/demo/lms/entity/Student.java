package com.demo.lms.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "STUDENT")
public class Student implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "S_ID")
	private Long sId;
	@Column(name = "FIRST_NAME")
	private String firstName;
	@Column(name = "LAST_NAME")
	private String lastName;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "MOBILE_NO")
	private BigInteger mobileNo;
	@Column(name = "EMAIL")
	private String email;
	@OneToMany(mappedBy = "student")
	private Set<Enrollment> enrollment;
	
	public Student() {
		super();
	}

	@Override
	public String toString() {
		return "Student [sId=" + sId + ", firstName=" + firstName + ", lastName=" + lastName + ", password=" + password
				+ ", mobileNo=" + mobileNo + ", email=" + email + ", enrollment=" + enrollment + "]";
	}	
}
